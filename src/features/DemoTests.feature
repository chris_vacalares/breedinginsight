@debug
Feature: Demo Tests
              Demo tests for BreedInsight

        Scenario: Validate user can see all traits
            Given a user logs with valid credentials
             When selecting the program Snacks from the lefthand naviation pane
              And selects Traits
             Then the user sees a page of All Traits

        Scenario: Validate user can see headers
            Given a user logs with valid credentials
             When selecting the program Snacks from the lefthand naviation pane
              And selects Traits
             Then the user sees a row of headers under All Traits
              And the headers are Name, Level, Method, and Scale

        Scenario: Validate user can see detail pane
            Given a user logs with valid credentials
             When selecting the program Snacks from the lefthand naviation pane
              And selects Traits
              And clicks Show details
             Then the user sees a a detail pane appear

        Scenario: Validate user can see details pane when selects row under headers
            Given a user logs with valid credentials
             When selecting the program Snacks from the lefthand naviation pane
              And selects Traits
              And selects a row under the headers
             Then the user sees a a detail pane appear

        Scenario: Error: Validate user can see details pane when selects row under headers
            Given a user logs with valid credentials
             When selecting the program Snacks from the lefthand naviation pane
              And selects Traits
              And selects a row under the headers
             Then the user not sees a a detail pane appear