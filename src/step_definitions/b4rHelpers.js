const { client } = require("nightwatch-api");
const b4rPage = client.page.b4rPage();

module.exports = {
  login: async function (user, password) {
    await client.url(client.launch_url);
    await b4rPage.click("@loginButton");

    let visible;
    await b4rPage.waitForElementVisible("#icon-3", ({ value }) => {
      visible = value;
    });

    if (visible) {
      await b4rPage.click("#icon-3");
    } else {
      await b4rPage.waitForElementVisible("@googleButton", 5000);
      await b4rPage.click("@googleButton");
    }

    //headless mode
    let headless = false;
    await b4rPage.waitForElementVisible(
      "@googleUsernameText",5000,
      false,
      ({ value }) => {
        headless = value;
      }
    );

    if (!headless) {
      console.log("headless here!");
      await b4rPage.setValue("#Email", user);
      await b4rPage.click("#next");
      await b4rPage.waitForElementVisible("#password");
      await b4rPage.setValue("#password", password);
      await b4rPage.click("#submit");
      
    } else {
      await b4rPage.setValue("@googleUsernameText", user);
      await b4rPage.click("@googleUsernameNextButton");
      await b4rPage.waitForElementVisible("@googlePasswordText");
      await b4rPage.setValue("@googlePasswordText", password);
      b4rPage.pause(2000);
      await b4rPage.click("@googlePasswordNextButton");
    }
  },

  capitalize: function (word) {
    return word
      .toLowerCase()
      .split(" ")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
  },
};
