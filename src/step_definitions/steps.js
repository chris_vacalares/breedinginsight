const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const page = client.page.page();
const b4rHelpers = require("./b4rHelpers.js");

//await b4rPage.perform(()=>{debugger;});

Given(/^a user logs with valid credentials$/, async () => {
  await page.navigate();
  await page.click("@iUnderstandButton");
  await page.click("@loginButton");
  await page.click("@orcidSignInButton");

  await page.setValue("@emailInput", "christian@mailinator.com");
  await page.setValue("@passwordInput", "cucumber1");
  await page.click("@signInButton");
});

When(
  /^selecting the program Snacks from the lefthand naviation pane$/,
  async () => {
    await page.click("@snacksButton");
  }
);

When(/^selects Traits$/, async () => {
  await page.click("@traitsButton");
});

Then(/^the user sees a page of All Traits$/, async () => {
  await page.assert.visible("@traitsTable");
});

Then(/^the user sees a row of headers under All Traits$/, async() => {
  await page.assert.visible("@traitsHeaderTable");
});

Then(/^the headers are Name, Level, Method, and Scale$/, async() => {
  await page.expect.element("#traitTableLabel th:nth-child(1)").text.contain("Name");
  await page.expect.element("#traitTableLabel th:nth-child(2)").text.contain("Level");
  await page.expect.element("#traitTableLabel th:nth-child(3)").text.contain("Method");
  await page.expect.element("#traitTableLabel th:nth-child(4)").text.contain("Scale");
});

When(/^clicks Show details$/, async() => {
	await page.click("#traitTableLabel tr:nth-child(1) a");
});

When(/^selects a row under the headers$/, async() => {
	await page.click("#traitTableLabel tr:nth-child(1) td:nth-child(1)");
});

Then(/^the user sees a a detail pane appear$/, async() => {
	await page.assert.visible("@traitsPane");
});

Then(/^the user not sees a a detail pane appear$/, async() => {
	await page.assert.not.visible("@traitsPane");
});

