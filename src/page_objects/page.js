module.exports = {
  url: "http://sandbox.breedinginsight.net/",
  // commands: [commands.commands],
  elements: {
    iUnderstandButton: "#app article.notification.is-marginless.is-warning button.button.is-dark",
    loginButton: "#app div.is-full-length main section div.column.is-three-fifths button",
    orcidSignInButton: "#connect-orcid-button",

    emailInput: "#mat-input-0",
    passwordInput: "#mat-input-1",

    signInButton:"#main app-form-sign-in form button",

    snacksButton: "#app > div.sidebarlayout a:nth-child(3)",

    traitsButton: "#sideMenu ul:nth-child(5) > li:nth-child(4) > a",

    traitsTable: "#traitTableLabel",

    traitsHeaderTable: "#traitTableLabel thead",

    traitsPane: "#traitTableLabel div.column.is-one-third-desktop.is-half-tablet.is-half-mobile.is-gapless.pl-0",

    googleButton:
      "#auth0-lock-container-1 > div > div.auth0-lock-center > form > div > div > div:nth-child(3) > span > div > div > div > div > div > div > div > div > div > div.auth0-lock-social-buttons-container > button > div.auth0-lock-social-button-icon",
    lastAccountButton:
      "#auth0-lock-container-1 > div > div.auth0-lock-center > form > div > div > div:nth-child(3) > span > div > div > div > div > div > div > div > div > button > div.auth0-lock-social-button-icon",

    googleUsernameText: "#identifierId",
    googleUsernameNextButton: "#identifierNext",
    googlePasswordText: "#password input",
    googlePasswordNextButton: "#passwordNext button",

    showLeftName: "#go-to-show-left-nav",

    experimentCreationMenu: "#slide-out li.parent-menu:nth-child(2) a:nth-child(1)",

    experimentTile: "#tools-widget-51-0-col_2-0- .material-icons.metrics-icon",

    createButton: "#create-expt-btn",

    experimentTypeSelect: {
      selector:
        "//*[@id='experiment_type']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    experimentTemplateSelect: {
      selector:
        "//*[@id='experiment_template']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    pipelineSelect: {
      selector:
        "//*[@id='PIPELINE-identification-2349']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    experimentNameField: "#EXPERIMENT_NAME-identification-2247",
    experimentObjectiveField: "#EXPERIMENT_OBJECTIVE-identification-2341",
    experimentYearField: "#EXPERIMENT_YEAR-identification-2339",
    stageSelect: {
      selector:
        "//*[@id='STAGE-identification-2337']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    evaluationSeasonSelect: {
      selector:
        "//*[@id='SEASON-identification-315']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    plantingSeasonField: "#PLANTING_SEASON-identification-2340",
    experimentStewardSelect: {
      selector:
        "//*[@id='EXPERIMENT_STEWARD-identification-2345']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    projectSelect: {
      selector:
        "//*[@id='PROJECT-identification-2338']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },

    saveButton: {
      selector: "//a[@id='next-btn'][contains(text(), 'Save')]",
      locateStrategy: "xpath",
    },
    nextButton: {
      selector: "//a[@id='next-btn'][contains(text(), 'Next')]",
      locateStrategy: "xpath",
    },

    addButton: "#add-entry-list-btn",

    b4rLogo: "#top-nav-bar > div > a",

    savedListButton: "a[title='Saved List']",
    inputListButton: "a[title='Input List']",

    designSelect: {
      selector:
        "//*[@id='design_select_id']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },

    numberOccurrencesField: "#nTrial",
    numberReplicatesField: "#nRep",
    numberPlotsPerBlockSelect: {
      selector:
        "//*[@id='select2-sBlk-container']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },
    rowsField: "#nFieldRow",
    plotsUntilField: "#nPlotBarrier",

    closeButton: "#add-entries-modal #cancel-save-btn",

    generateDesignButton: "#submit_design_btn",

    experimentFilterField: "input[name='ExperimentSearch[experimentName]']",
    rowsSelect: {
      selector:
        "//*[@id='nFieldRow']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },

    plotsUntilSelect: {
      selector:
        "//*[@id='select2-nPlotBarrier-container']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },

    noRowsPerRepSelect:{
      selector:
        "//*[@id='nRowPerRep']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },

    rowBlocksSelect:{
      selector:
        "//*[@id='select2-nRowBlk-container']/..//span[@class='select2-selection__arrow']",
      locateStrategy: "xpath",
    },

    noBlocksPerSelect:{
      selector:
      "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
    locateStrategy: "xpath",
    },

    noBlocksPerSelect:{
      selector:
      "//*[@id='nBlk']/..//span[@class='select2-selection__arrow']",
    locateStrategy: "xpath",
    },

    noRowsPerBlock:{
      selector:
      "//*[@id='nRowPerBlk']/..//span[@class='select2-selection__arrow']",
    locateStrategy: "xpath",
    },

    //checkbox, go to the row switch
    shapeCheckbox: {selector:"//*[@id='genLayout']", locateStrategy:"xpath"},
    randomizeCheckbox: {selector:"//*[@id='rand1']", locateStrategy:"xpath"},

    layoutPanel: "#design-layout-panel",
  },
};

